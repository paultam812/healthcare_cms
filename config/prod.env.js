module.exports = {
  NODE_ENV: '"production"',
  ENV_CONFIG: '"prod"',
  // BASE_API: '"https://test.ftinnotech.com/ci-rest"',
  // IMAGE_DOMAIN: '"https://test.ftinnotech.com/ci-rest/"'
  BASE_API: '"http://localhost:8888/tms_rest/"',
  IMAGE_DOMAIN: '"http://localhost:8888/tms_rest/"'
}
