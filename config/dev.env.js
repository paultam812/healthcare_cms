module.exports = {
  NODE_ENV: '"development"',
  ENV_CONFIG: '"dev"',
  // BASE_API: '"https://test.ftinnotech.com/tms_rest"',
  // IMAGE_DOMAIN: '"https://test.ftinnotech.com/tms_rest/"'
  BASE_API: '"http://localhost:8888/tms_rest/"',
  IMAGE_DOMAIN: '"http://localhost:8888/tms_rest/"'
}
