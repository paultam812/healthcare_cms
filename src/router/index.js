import Vue from 'vue'
import Router from 'vue-router'
import customerRouter from './modules/customer'
import staffRouter from './modules/staff'
import couponRouter from './modules/coupon'
import invoiceRouter from './modules/invoice'
import branchRouter from './modules/branch'
import offerRouter from './modules/offer'
import Cookies from 'js-cookie'

Vue.use(Router)

/* Layout */
import Layout from '@/views/layout/Layout'

/* Router Modules */
// import localizationRouter from './modules/localization'

/** note: sub-menu only appear when children.length>=1
 *  detail see  https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 **/

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirect in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    roles: ['admin','editor']    will control the page roles (you can set multiple roles)
    title: 'title'               the name show in sub-menu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    noCache: true                if true, the page will no be cached(default is false)
    breadcrumb: false            if false, the item will hidden in breadcrumb(default is true)
    affix: true                  if true, the tag will affix in the tags-view
  }
**/
export const constantRouterMap = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path*',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/authredirect'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/errorPage/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/errorPage/401'),
    hidden: true
  }
]

export default new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})

export const asyncRouterMap = [
  {
    path: '',
    component: Layout,
    name:"",
    redirect: '/customer/list' ,
  },
  // {
  //   path: 'user',
  //   component: Layout,
  //   redirect: 'user',
  //   children: [
  //     {
  //       path: 'user',
  //       component: () => import('@/views/user/User'),
  //       name: 'User',
  //       meta: { title: 'user', icon: 'user', noCache: true }
  //     }
  //   ]
  // },
  staffRouter,
  customerRouter,
  couponRouter,
  invoiceRouter,
  branchRouter,
  offerRouter,
  {
    path: '/service',
    component: Layout,
    name: 'Service',
    meta: { title: 'service', icon: 'list' },
    children: [
      {
        path: 'bodycheck',
        component: () => import('@/views/service/BodyCheck'),
        name: 'BodyCheck',
        meta: { title: 'bodyCheck', icon: 'list', noCache: true }
      },
      {
        path: 'plan',
        component: () => import('@/views/service/Plan'),
        name: 'Plan',
        meta: { title: 'plan', icon: 'list', noCache: true }
      },
      {
        path: 'item',
        component: () => import('@/views/service/Item'),
        name: 'item',
        meta: { title: 'item', icon: 'list', noCache: true }
      },
      {
        path: 'topic',
        component: () => import('@/views/service/Topic'),
        name: 'Topic',
        meta: { title: 'topic', icon: 'list', noCache: true }
      },

    ]
  },
  
  // {
  //   path: '/sample',
  //   component: Layout,
  //   redirect: '/sample/sample1',
  //   name: 'Sample',
  //   meta: { title: 'sample', icon: 'list' },
  //   children: [
  //     {
  //       path: 'sample1',
  //       component: () => import('@/views/sample/Sample'),
  //       name: 'Sample1',
  //       meta: { title: 'sample1' }
  //     },
  //     {
  //       path: 'sample2',
  //       component: () => import('@/views/sample/Sample'),
  //       name: 'Sample2',
  //       meta: { title: 'sample2' }
  //     }
  //   ]
  // },

  { path: '*', redirect: '/404', hidden: true }
]
