/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/views/layout/Layout'

const couponRouter = {
  path: '/coupon',
  component: Layout,
  children: [
    {
      path: 'list',
      component: () => import('@/views/coupon/Coupon'),
      name: 'Coupon',
      meta: { title: 'coupon', icon: 'user', noCache: true }
    }
  ]
}

export default couponRouter
