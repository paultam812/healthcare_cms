/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/views/layout/Layout'

const customerRouter = {
  path: '/customer',
  component: Layout,
  children: [
    {
      path: 'list',
      component: () => import('@/views/customer/Customer'),
      name: 'Customer',
      meta: { title: 'customer', icon: 'user', noCache: true }
    }
  ]
}

export default customerRouter
