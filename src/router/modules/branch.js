/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/views/layout/Layout'

const branchRouter = {
  path: '/branch',
  component: Layout,
  children: [
    {
      path: 'list',
      component: () => import('@/views/branch/Branch'),
      name: 'Customer',
      meta: { title: 'branch', icon: 'user', noCache: true }
    }
  ]
}

export default branchRouter
