/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/views/layout/Layout'

const offerRouter = {
  path: '/offer',
  component: Layout,
  children: [
    {
      path: 'list',
      component: () => import('@/views/offer/Offer'),
      name: 'Offer',
      meta: { title: 'offer', icon: 'user', noCache: true }
    }
  ]
}

export default offerRouter
