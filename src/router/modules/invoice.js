/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/views/layout/Layout'

const invoiceRouter = {
  path: '/invoice',
  component: Layout,
  children: [
    {
      path: 'list',
      component: () => import('@/views/invoice/Invoice'),
      name: 'Invoice',
      meta: { title: 'invoice', icon: 'user', noCache: true }
    }
  ]
}

export default invoiceRouter
