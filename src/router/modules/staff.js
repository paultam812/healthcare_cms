/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/views/layout/Layout'

const staffRouter = {
  path: '/staff',
  component: Layout,
  children: [
    {
      path: 'list',
      component: () => import('@/views/staff/Staff'),
      name: 'Customer',
      meta: { title: 'staff', icon: 'user', noCache: true }
    }
  ]
}

export default staffRouter
