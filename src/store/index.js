import Vue from 'vue'
import Vuex from 'vuex'
import app from './modules/app'
import permission from './modules/permission'
import user from './modules/user'
import dashboard from './modules/dashboard'
import auth from './modules/auth'
import getters from './getters'

import staff from './modules/staff'
import file from './modules/file'
import customer from './modules/customer'
import coupon from './modules/coupon'
import topic from './modules/topic'
import item from './modules/item'
import plan from './modules/plan'
import offer from './modules/offer'
import discountPlan from './modules/discountPlan'
import discountItem from './modules/discountItem'
import invoice from './modules/invoice'
import bodyCheck from './modules/bodyCheck'
import branch from './modules/branch'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    permission,
    auth,
    user,
    staff,
    file,
    customer,
    dashboard,
    coupon,
    item,
    topic,
    plan,
    offer,
    discountPlan,
    discountItem,
    invoice,
    bodyCheck,
    branch
  },
  getters
})

export default store
