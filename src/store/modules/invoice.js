import request from '@/utils/request'
import request2 from '@/utils/request2'

const invoice = {
  state: {
    currentMapID: 0
  },

  mutations: {
    setCurrentMapID(state, val) {
      state.currentMapID = val
    }
  },

  actions: {
    getInvoiceList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Invoice/list',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    getInvoiceNoList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Invoice/reuseInvoiceList',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    getInvoiceDetail({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Invoice/detail',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    getDebitHistory({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Invoice/debitList',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    exportInvoice({ commit }, params) {
      return new Promise((resolve, reject) => {
        request2({
          url: '/Invoice/exportInvoice',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    exportIndividualInvoice({ commit }, params) {
      return new Promise((resolve, reject) => {
        request2({
          url: '/Invoice/exportIndividualInvoice',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    exportCouponInvoice({ commit }, params) {
      return new Promise((resolve, reject) => {
        request2({
          url: '/Invoice/exportCouponInvoice',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    exportInvoiceReport({ commit }, params) {
      return new Promise((resolve, reject) => {
        request2({
          url: '/Invoice/exportInCsv',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    downloadReceipt({ commit }, params) {
      return new Promise((resolve, reject) => {
        request2({
          url: '/Invoice/exportReceipt',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    viewInvoice({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Customer/viewInvoice',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    createInvoice({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Invoice/createInvoice',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    editInvoice({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Invoice/editInvoice',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    deleteInvoice({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Invoice/deleteInvoice',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    releaseInvoice({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Invoice/releaseInvoice',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    createDebit({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Invoice/createDebit',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    deleteDebit({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Invoice/deleteDebit',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    }
  }
}

export default invoice
