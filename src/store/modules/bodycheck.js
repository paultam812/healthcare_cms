import request from '@/utils/request'
import request2 from '@/utils/request2'
import request3 from '@/utils/request3'

const bodyCheck = {
  state: {
    currentMapID: 0
  },

  mutations: {
    setCurrentMapID(state, val) {
      state.currentMapID = val
    }
  },

  actions: {
    getBodyCheckList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/bodyCheckList',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    getBodyCheckDetail({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/bodyCheckDetail',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    createBodyCheck({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/createBodyCheck',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    editBodyCheck({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/editBodyCheck',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    cancelBodyCheck({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/deleteBodyCheck',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    removeBodyCheck({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/removeBodyCheck',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    exportBodyCheck({ commit }, params) {
      return new Promise((resolve, reject) => {
        request2({
          url: '/Service/exportBodyCheckInCSV',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    exportLogSheet({ commit }, params) {
      return new Promise((resolve, reject) => {
        request2({
          url: '/Service/exportBodyCheck',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    setFinishBodyCheck({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/markDone',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    restoreBodyCheck({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/restoreBodyCheck',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    editBodyCheckRemark({ commit }, params) {
      return new Promise((resolve, reject) => {
        request3({
          url: '/Service/remark',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    editBodyCheckGotGifts({ commit }, params) {
      return new Promise((resolve, reject) => {
        request3({
          url: '/Service/gotGift',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    setBodyCheckStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request3({
          url: '/Service/bodyCheckStatus',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    downloadLogSheet({ commit }, params) {
      return new Promise((resolve, reject) => {
        request2({
          url: '/Invoice/exportIndividualLogSheet',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    }
  }
}

export default bodyCheck
