import request from '@/utils/request'
import request1 from '@/utils/request1'
import request2 from '@/utils/request2'

const file = {
  state: {
    currentMapID: 0
  },

  mutations: {
    setCurrentMapID(state, val) {
      state.currentMapID = val
    }
  },

  actions: {
    // new 1.
    viewCustomerFile({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Customer/viewFile',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    downloadFile({ commit }, params) {
      return new Promise((resolve, reject) => {
        request2({
          url: '/Customer/downloadFile',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    // new 2.
    deleteStaff({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Staff/deleteStaff',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    deleteFile({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/customer/deleteFile',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    batchUpload({ commit }, params) {
      return new Promise((resolve, reject) => {
        request1({
          url: '/file/batchUpload',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
  }
}

export default file
