import request from '@/utils/request'

const staff = {
  state: {
    currentMapID: 0
  },

  mutations: {
    setCurrentMapID(state, val) {
      state.currentMapID = val
    }
  },

  actions: {
    // new 1.
    getStaffList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Staff/list',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    getStaffDetail({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Staff/detail',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    // new 2.
    deleteStaff({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Staff/deleteStaff',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    // new 3.
    createStaff({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Staff/createStaff',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    // new 4.
    editStaff({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Staff/editStaff',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
  }
}

export default staff
