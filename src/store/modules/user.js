import request from '@/utils/request'

const user = {
  state: {
    currentMapID: 0
  },

  mutations: {
    setCurrentMapID(state, val) {
      state.currentMapID = val
    }
  },

  actions: {
    // new 1.
    getManagerList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/staff/getNoneStaffs',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    // new 2.
    deleteUser({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/User/deleteUsers',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    // new 3.
    createUser({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/User/createUser',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    // new 4.
    editUser({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/User/editUser',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },

    searchUser({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/user/search',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    editPassword({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/user/editPassword',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    }
  }
}

export default user
