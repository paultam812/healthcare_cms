import request from '@/utils/request'
import request1 from '@/utils/request1'
import request2 from '@/utils/request2'

const offer = {
  state: {
    currentMapID: 0
  },

  mutations: {
    
  },

  actions: {

    getOfferList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Offer/offerList',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    getOfferDetail({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Offer/offerDetail',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    eligibleList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Offer/eligibleList',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    viewOffer({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Customer/offer',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    assignOffer({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Offer/assignOffer',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    editOffer({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Offer/editOffer',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    deleteOffer({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Offer/deleteOffer',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    exportOfferTwo({ commit }, params) {
      return new Promise((resolve, reject) => {
        request2({
          url: '/Offer/exportReceivers',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    exportOfferThree({ commit }, params) {
      return new Promise((resolve, reject) => {
        request2({
          url: '/Offer/exportReceivers',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
  }
}

export default offer
