import request from '@/utils/request'
import request1 from '@/utils/request1'

const expense = {
  state: {
    currentMapID: 0
  },

  mutations: {
    setCurrentMapID(state, val) {
      state.currentMapID = val
    }
  },

  actions: {
    getCouponList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Coupon/list',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    getCouponDetail({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Coupon/detail',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    createCoupon({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Coupon/createCoupon',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    editCoupon({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Coupon/editCoupon',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    deleteCoupon({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Coupon/deleteCoupon',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    getCouponDetail({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Coupon/detail',
          method: 'get',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
  }
}

export default expense
