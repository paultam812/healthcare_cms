import request from '@/utils/request'
import request1 from '@/utils/request1'

const discountPlan = {
  state: {
    currentMapID: 0
  },

  mutations: {
    setCurrentMapID(state, val) {
      state.currentMapID = val
    }
  },

  actions: {
    getDiscountPlanList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/discountPriceList',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    getDiscountPlanDetail({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/discountPriceDetail',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    createDiscountPlan({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/createDiscountPrice',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    editDiscountPlan({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/editDiscountPrice',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    deleteDiscountPlan({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/deleteDiscountPrice',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    
  }
}

export default discountPlan
