import request from '@/utils/request'
import request1 from '@/utils/request1'

const topic = {
  state: {
    currentMapID: 0
  },

  mutations: {
    setCurrentMapID(state, val) {
      state.currentMapID = val
    }
  },

  actions: {
    getTopics({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/topicList',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    getTopicDetail({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/topicDetail',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    createTopic({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/createTopic',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    editTopic({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/editTopic',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    deleteTopic({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/deleteTopic',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    
  }
}

export default topic
