import request from '@/utils/request'
import request1 from '@/utils/request1'

const plan = {
  state: {
    currentMapID: 0
  },

  mutations: {
    setCurrentMapID(state, val) {
      state.currentMapID = val
    }
  },

  actions: {
    getPlanList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/planList',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    getPlanDetail({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/planDetail',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    createPlan({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/createPlan',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    editPlan({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/editPlan',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    deletePlan({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/deletePlan',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    
  }
}

export default plan
