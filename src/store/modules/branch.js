import request from '@/utils/request'
import request1 from '@/utils/request1'

const expense = {
  state: {
    currentMapID: 0
  },

  mutations: {
    setCurrentMapID(state, val) {
      state.currentMapID = val
    }
  },

  actions: {
    getBranchList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Branch/list',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    getBranchDetail({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Branch/detail',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    createBranch({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Branch/createBranch',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    editBranch({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Branch/editBranch',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    deleteBranch({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Branch/deleteBranch',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    
  }
}

export default expense
