import request from '@/utils/request'
import request1 from '@/utils/request1'

const item = {
  state: {
    currentMapID: 0
  },

  mutations: {
    setCurrentMapID(state, val) {
      state.currentMapID = val
    }
  },

  actions: {
    getItemList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/itemList',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    getItemCode({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/itemCode',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    getItemDetail({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/itemDetail',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    createItem({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/createItem',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    editItem({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/editItem',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    deleteItem({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/deleteItem',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    
  }
}

export default item
