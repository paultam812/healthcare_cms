import request from '@/utils/request'
import request1 from '@/utils/request1'

const discountItem = {
  state: {
    currentMapID: 0
  },

  mutations: {
    setCurrentMapID(state, val) {
      state.currentMapID = val
    }
  },

  actions: {
    getDiscountItemList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/discountPriceListForItem',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    getDiscountItemDetail({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/discountPriceDetailForItem',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    createDiscountItem({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/createDiscountPriceForItem',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    editDiscountItem({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/editDiscountPriceForItem',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    deleteDiscountItem({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Service/deleteDiscountPriceForItem',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    
  }
}

export default discountItem
