import request from '@/utils/request'
import request1 from '@/utils/request1'
import request4 from '@/utils/request4'
import request3 from '@/utils/request3'

const customer = {
  state: {
    currentMapID: 0
  },

  mutations: {
    setCurrentMapID(state, val) {
      state.currentMapID = val
    }
  },

  actions: {
    // new 1.
    getCustomerList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Customer/customerList',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    getCustomerDetail({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Customer/detail',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    viewBodyCheck({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Customer/viewBodyCheck',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    createIndividualCustomer({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Customer/createIndividual',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    editIndividualCustomer({ commit }, params) {
      return new Promise((resolve, reject) => {
        request4({
          url: '/Customer/editIndividual',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    createCorporateCustomer({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Customer/createCorporate',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    editCorporateCustomer({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Customer/editCorporate',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    deleteCustomer({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Customer/delete',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    uploadFile({ commit }, params) {
      return new Promise((resolve, reject) => {
        request1({
          url: '/Customer/uploadFile',
          method: 'post',
          data: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    viewPlan({ commit }, params) {
      return new Promise((resolve, reject) => {
        request4({
          url: '/Customer/viewPlan',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    viewItem({ commit }, params) {
      return new Promise((resolve, reject) => {
        request4({
          url: '/Customer/viewItem',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    viewCoupon({ commit }, params) {
      return new Promise((resolve, reject) => {
        request4({
          url: '/Customer/viewCoupon',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    duplicateName({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Customer/duplicateName',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    checkIDExistence({ commit }, params) {
      return new Promise((resolve, reject) => {
        request3({
          url: '/Customer/hkid',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    // new 4.
  }
}

export default customer
