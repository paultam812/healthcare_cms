import request from '@/utils/request'

const dashboard = {
  state: {
    currentMapID: 0
  },

  mutations: {
    setCurrentMapID(state, val) {
      state.currentMapID = val
    }
  },

  actions: {
    getRevenueSummary({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Statistic/getRevenue',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    getCompanyOptions({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Code/getCompanies',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    getClientSummary({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Statistic/getClientStats',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    getStaffSummary({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Statistic/getStaffStats',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
    getTaskSummary({ commit }, params) {
      return new Promise((resolve, reject) => {
        request({
          url: '/Statistic/getTaskStats',
          method: 'get',
          params: params
        }).then(response => resolve(response)).catch(error => reject(error))
      })
    },
  }
}

export default dashboard
